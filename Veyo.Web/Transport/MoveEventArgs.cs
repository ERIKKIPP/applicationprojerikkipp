﻿using System;

namespace Transport
{
    internal sealed class MoveEventArgs : EventArgs
    {
        public MoveEventArgs()
        {

        }

        public Tuple<int, int> NewPosition { get; set; }
    }
}