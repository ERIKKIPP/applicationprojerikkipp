﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transport
{
    internal abstract class Car
    {
        public event EventHandler<MoveEventArgs> CarMoved;

        public event EventHandler<MoveEventArgs> PickedUpPassenger;

        public int XPos { get; protected set; }
        public int YPos { get; protected set; }
        public Passenger Passenger { get; private set; }
        public City City { get; private set; }

        public Car(int xPos, int yPos, City city, Passenger passenger, IMoveBehavior moveBehavior)
        {
            XPos = xPos;
            YPos = yPos;
            City = city;
            Passenger = passenger;
            MoveBehavior = moveBehavior;
        }

        protected void WritePositionToConsole()
        {
            Console.WriteLine(string.Format($"{this.GetType().Name} moved to x - {XPos} y - {YPos}"));

            if (CarMoved != null)
            {
                CarMoved(this, new MoveEventArgs() { NewPosition = new Tuple<int, int>(XPos, YPos) });
            }

        }

        public void PickupPassenger(Passenger passenger)
        {
            Passenger = passenger;

            if (PickedUpPassenger != null)
            {
                PickedUpPassenger(this, new MoveEventArgs() { NewPosition = new Tuple<int, int>(XPos, YPos) });
            }
        }

        public IMoveBehavior MoveBehavior { get; set; }

        public abstract void MoveUp();

        public abstract void MoveDown();

        public abstract void MoveRight();

        public abstract void MoveLeft();
    }
}
