﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transport
{
    internal enum MoveDirection
    {
        North,
        South,
        East,
        West
    }
}
