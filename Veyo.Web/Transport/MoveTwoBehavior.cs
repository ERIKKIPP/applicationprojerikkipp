﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transport
{
    internal sealed class MoveTwoBehavior : IMoveBehavior
    {
        public int MoveDown(int yPos)
        {
            return yPos - MoveAmount;
        }

        public int MoveLeft(int xPos)
        {
            return xPos - MoveAmount;
        }

        public int MoveRight(int xPos)
        {
            return xPos + MoveAmount;
        }

        public int MoveUp(int yPos)
        {
            return yPos + MoveAmount;
        }

        public int MoveAmount { get; } = 2;
    }
}
