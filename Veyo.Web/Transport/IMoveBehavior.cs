﻿namespace Transport
{
    internal interface IMoveBehavior
    {
        int MoveUp(int yPos);

        int MoveDown(int yPos);

        int MoveRight(int xPos);

        int MoveLeft(int xPos);

        int MoveAmount { get; }
    }
}