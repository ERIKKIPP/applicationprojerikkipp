﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transport
{
    public static class Transportation
    {
        private static Random s_rand = new Random();

        public static IList<PathData> GetPath(int citySize)
        {
            bool hasPassenger = false;
            IList<PathData> list = new List<PathData>();
            City myCity = new City(citySize, citySize);
            Car car = myCity.AddCarToCity(s_rand.Next(citySize - 1), s_rand.Next(citySize - 1), new  MoveOneBehavior());
            Passenger passenger = myCity.AddPassengerToCity(s_rand.Next(citySize - 1), s_rand.Next(citySize - 1), s_rand.Next(citySize - 1), s_rand.Next(citySize - 1));

            list.Add(new PathData() { PositionX = car.XPos, PositionY = car.YPos, HasPassenger = hasPassenger });

            car.CarMoved += (sender, e) =>
            {
                list.Add(new PathData() { PositionX = e.NewPosition.Item1, PositionY = e.NewPosition.Item2, HasPassenger = hasPassenger });
            };

            car.PickedUpPassenger += (sender, e) =>
            {
                hasPassenger = true;

                PathData p = list.FirstOrDefault(s => s.PositionX == e.NewPosition.Item1 && s.PositionY == e.NewPosition.Item2);
                p.HasPassenger = true;
            };

            while (!passenger.IsAtDestination())
            {
                Tick(car, passenger);
            }

            return list;
        }

        private static void SlowDown(Car car, MoveDirection dir)
        {
            IMoveBehavior move = car.MoveBehavior;
            car.MoveBehavior = new MoveOneBehavior();//downshift

            switch (dir)
            {
                case MoveDirection.North:
                    car.MoveUp();
                    break;
                case MoveDirection.South:
                    car.MoveDown();
                    break;
                case MoveDirection.East:
                    car.MoveRight();
                    break;
                case MoveDirection.West:
                    car.MoveLeft();
                    break;
                default:
                    break;
            }

            car.MoveBehavior = move;//upshift
        }

        /// <summary>
        /// Takes one action (move the car one spot or pick up the passenger).
        /// </summary>
        /// <param name="car">The car to move</param>
        /// <param name="passenger">The passenger to pick up</param>
        private static void Tick(Car car, Passenger passenger)
        {
            //no passenger so let's pick up
            if (car.Passenger == null)
            {
                if (car.XPos < passenger.StartingXPos)
                {
                    if (passenger.StartingXPos - car.XPos < car.MoveBehavior.MoveAmount)
                    {
                        SlowDown(car, MoveDirection.East);
                    }
                    else
                    {
                        car.MoveRight();
                    }
                }
                else if (car.XPos > passenger.StartingXPos)
                {
                    if (car.XPos - passenger.StartingXPos < car.MoveBehavior.MoveAmount)
                    {
                        SlowDown(car, MoveDirection.West);
                    }
                    else
                    {
                        car.MoveLeft();
                    }
                }
                else if (car.YPos < passenger.StartingYPos)
                {
                    if (passenger.StartingYPos - car.YPos < car.MoveBehavior.MoveAmount)
                    {
                        SlowDown(car, MoveDirection.North);
                    }
                    else
                    {
                        car.MoveUp();
                    }
                }
                else if (car.YPos > passenger.StartingYPos)
                {
                    if (car.YPos - passenger.StartingYPos < car.MoveBehavior.MoveAmount)
                    {
                        SlowDown(car, MoveDirection.South);
                    }
                    else
                    {
                        car.MoveDown();
                    }
                }
                else
                {
                    passenger.GetInCar(car);
                }
            }
            else
            {
                //picked up passenger so let's move to dest
                if (car.XPos < passenger.DestinationXPos)
                {
                    if (passenger.DestinationXPos - car.XPos < car.MoveBehavior.MoveAmount)
                    {
                        SlowDown(car, MoveDirection.East);
                    }
                    else
                    {
                        car.MoveRight();
                    }

                }
                else if (car.XPos > passenger.DestinationXPos)
                {
                    if (car.XPos - passenger.DestinationXPos < car.MoveBehavior.MoveAmount)
                    {
                        SlowDown(car, MoveDirection.West);
                    }
                    else
                    {
                        car.MoveLeft();
                    }
                }
                else if (car.YPos < passenger.DestinationYPos)
                {
                    if (passenger.DestinationYPos - car.YPos < car.MoveBehavior.MoveAmount)
                    {
                        SlowDown(car, MoveDirection.North);
                    }
                    else
                    {
                        car.MoveUp();
                    }
                }
                else if (car.YPos > passenger.DestinationYPos)
                {
                    if (car.YPos - passenger.StartingYPos < car.MoveBehavior.MoveAmount)
                    {
                        SlowDown(car, MoveDirection.South);
                    }
                    else
                    {
                        car.MoveDown();
                    }
                }
            }
        }
    }
}
