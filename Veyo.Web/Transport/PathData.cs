﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transport
{
    public sealed class PathData
    {
        public PathData()
        {
           
        }

        public int PositionX { get; set; }

        public int PositionY { get; set; }

        public bool HasPassenger { get; set; }
    }
}
