﻿$(function () {

    var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;

    function AnimateLine(context, x1, y1, x2, y2, destX, destY, color) {

        context.beginPath();
        context.lineWidth = "5";
        context.strokeStyle = color;
        context.moveTo(x1, y1);

        if (x2 != destX) {
            if (x2 < destX) {
                x2++;
            }
            else {
                x2--;
            }
        }

        if (y2 != destY) {
            if (y2 < destY) {
                y2++;
            }
            else {
                y2--;
            }
        }

        context.lineTo(x2, y2);
        context.stroke(); // Draw it  

        if (x2 != destX || y2 != destY) {
            requestAnimationFrame(function () {
                AnimateLine(context, x1, y1, x2, y2, destX, destY, color);
            });
        }
    };

    $("#pathButton").click(function (e) {

        $("#pathButton").attr("disabled", "disabled");

        $.ajax({
            url: "/api/transport",
            success: function (results) {

                var canvas = document.getElementById('myCanvas');
                var context = canvas.getContext('2d');

                context.clearRect(0, 0, canvas.width, canvas.height);

                for (var res = 0; res < results.length - 1; res++) {

                    objA = results[res];
                    objB = results[res + 1];

                    AnimateLine(context, objA.X * 10, objA.Y * 10, objA.X * 10, objA.Y * 10, objB.X * 10, objB.Y * 10, objA.HasPassenger ? "red" : "blue");
                }
            },
            error: function (xhr, status, error) {

            },
            cache: false
        });

        $("#pathButton").removeAttr("disabled");
    });
});