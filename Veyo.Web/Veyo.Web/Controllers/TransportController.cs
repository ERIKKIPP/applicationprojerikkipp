﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Transport;
using Veyo.Web.Models;

namespace Veyo.Web.Controllers
{
    public class TransportController : ApiController
    {
        // GET api/values

        public IEnumerable<PathModel> Get()
        {
            List<PathModel> path = new List<PathModel>();
            IList<PathData> list = Transportation.GetPath(20);

            foreach (var item in list)
            {
                path.Add(new PathModel
                {
                    X = item.PositionX,
                    Y = item.PositionY,
                    HasPassenger = item.HasPassenger
                });
            }

            return path;
        }


    }
}
