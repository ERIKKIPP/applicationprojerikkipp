﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Veyo.Web.Models
{
    public sealed class PathModel
    {
        public PathModel()
        {

        }

        public int X { get; set; }

        public int Y { get; set; }

        public bool HasPassenger { get; set; }
    }
}