﻿using System;

namespace TransportProj
{
    public class Sedan : Car
    {
        public Sedan(int xPos, int yPos, City city, Passenger passenger, IMoveBehavior moveBehavior)
            : base(xPos, yPos, city, passenger, moveBehavior)
        {
        }

        public override void MoveUp()
        {
            if (YPos < City.YMax)
            {
                YPos = MoveBehavior.MoveUp(YPos);//++
                WritePositionToConsole();
            }
        }

        public override void MoveDown()
        {
            if (YPos > 0)
            {
                YPos = MoveBehavior.MoveDown(YPos); //--
                WritePositionToConsole();
            }
        }

        public override void MoveRight()
        {
            if (XPos < City.XMax)
            {
                XPos = MoveBehavior.MoveRight(XPos);//++;
                WritePositionToConsole();
            }
        }

        public override void MoveLeft()
        {
            if (XPos > 0)
            {
                XPos = MoveBehavior.MoveLeft(XPos); //--
                WritePositionToConsole();
            }
        }
        
    }
}
