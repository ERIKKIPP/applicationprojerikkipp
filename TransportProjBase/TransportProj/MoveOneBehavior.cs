﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj
{
    public sealed class MoveOneBehavior : IMoveBehavior
    {
        public MoveOneBehavior()
        {

        }

        public int MoveAmount { get; } = 1;

        public int MoveDown(int yPos)
        {
            return yPos -= MoveAmount;
        }

        public int MoveLeft(int xPos)
        {
            return xPos -= MoveAmount;
        }

        public int MoveRight(int xPos)
        {
            return xPos += MoveAmount;
        }

        public int MoveUp(int yPos)
        {
            return yPos += MoveAmount;
        }
    }
}
