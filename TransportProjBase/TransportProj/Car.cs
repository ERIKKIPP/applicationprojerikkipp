﻿using System;

namespace TransportProj
{
    public abstract class Car
    {
        public event EventHandler<MoveEventArgs> CarMoved;

        public int XPos { get; protected set; }
        public int YPos { get; protected set; }
        public Passenger Passenger { get; private set; }
        public City City { get; private set; }

        public Car(int xPos, int yPos, City city, Passenger passenger, IMoveBehavior moveBehavior)
        {
            XPos = xPos;
            YPos = yPos;
            City = city;
            Passenger = passenger;
            MoveBehavior = moveBehavior;
        }

        protected void WritePositionToConsole()
        {
            Console.WriteLine(string.Format($"{this.GetType().Name} moved to x - {XPos} y - {YPos}"));

            if (CarMoved != null)
            {
                CarMoved(this, new MoveEventArgs() { NewPosition = new Tuple<int, int>(XPos, YPos) });
            }

        }

        public void PickupPassenger(Passenger passenger)
        {
            Passenger = passenger;
        }

        public IMoveBehavior MoveBehavior { get; set; }

        public abstract void MoveUp();

        public abstract void MoveDown();

        public abstract void MoveRight();

        public abstract void MoveLeft();
    }
}
