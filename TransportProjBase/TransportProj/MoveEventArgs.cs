﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj
{
    public sealed class MoveEventArgs : EventArgs
    {
        public MoveEventArgs()
        {

        }
        
        public Tuple<int, int> NewPosition { get; set; }
    }
}
