﻿using System;

namespace TransportProj
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            int cityLength = 10;
            int cityWidth = 10;

            City myCity = new City(cityLength, cityWidth);
            Car car = myCity.AddCarToCity(rand.Next(cityLength - 1), rand.Next(cityWidth - 1), new MoveTwoBehavior());
            Passenger passenger = myCity.AddPassengerToCity(rand.Next(cityLength - 1), rand.Next(cityWidth - 1), rand.Next(cityLength - 1), rand.Next(cityWidth - 1));

            while (!passenger.IsAtDestination())
            {
                Tick(car, passenger);
            }

            Console.ReadLine();

            return;
        }

        private enum MoveDirection
        {
            North,
            South,
            East,
            West
        }

        private static void SlowDown(Car car, MoveDirection dir)
        {
            IMoveBehavior move = car.MoveBehavior;
            car.MoveBehavior = new MoveOneBehavior();//downshift

            switch (dir)
            {
                case MoveDirection.North:
                    car.MoveUp();
                    break;
                case MoveDirection.South:
                    car.MoveDown();
                    break;
                case MoveDirection.East:
                    car.MoveRight();
                    break;
                case MoveDirection.West:
                    car.MoveLeft();
                    break;
                default:
                    break;
            }

            car.MoveBehavior = move;//upshift
        }

        /// <summary>
        /// Takes one action (move the car one spot or pick up the passenger).
        /// </summary>
        /// <param name="car">The car to move</param>
        /// <param name="passenger">The passenger to pick up</param>
        private static void Tick(Car car, Passenger passenger)
        {
            //no passenger so let's pick up
            if (car.Passenger == null)
            {
                if (car.XPos < passenger.StartingXPos)
                {
                    if (passenger.StartingXPos - car.XPos < car.MoveBehavior.MoveAmount)
                    {
                        SlowDown(car, MoveDirection.East);
                    }
                    else
                    {
                        car.MoveRight();
                    }
                }
                else if (car.XPos > passenger.StartingXPos)
                {
                    if (car.XPos - passenger.StartingXPos < car.MoveBehavior.MoveAmount)
                    {
                        SlowDown(car, MoveDirection.West);
                    }
                    else
                    {
                        car.MoveLeft();
                    }
                }
                else if (car.YPos < passenger.StartingYPos)
                {
                    if (passenger.StartingYPos - car.YPos < car.MoveBehavior.MoveAmount)
                    {
                        SlowDown(car, MoveDirection.North);
                    }
                    else
                    {
                        car.MoveUp();
                    }
                }
                else if (car.YPos > passenger.StartingYPos)
                {
                    if (car.YPos - passenger.StartingYPos < car.MoveBehavior.MoveAmount)
                    {
                        SlowDown(car, MoveDirection.South);
                    }
                    else
                    {
                        car.MoveDown();
                    }
                }
                else
                {
                    passenger.GetInCar(car);
                }
            }
            else
            {
                //picked up passenger so let's move to dest
                if (car.XPos < passenger.DestinationXPos)
                {
                    if (passenger.DestinationXPos - car.XPos < car.MoveBehavior.MoveAmount)
                    {
                        SlowDown(car, MoveDirection.East);
                    }
                    else
                    {
                        car.MoveRight();
                    }

                }
                else if (car.XPos > passenger.DestinationXPos)
                {
                    if (car.XPos - passenger.DestinationXPos < car.MoveBehavior.MoveAmount)
                    {
                        SlowDown(car, MoveDirection.West);
                    }
                    else
                    {
                        car.MoveLeft();
                    }
                }
                else if (car.YPos < passenger.DestinationYPos)
                {
                    if (passenger.DestinationYPos - car.YPos < car.MoveBehavior.MoveAmount)
                    {
                        SlowDown(car, MoveDirection.North);
                    }
                    else
                    {
                        car.MoveUp();
                    }
                }
                else if (car.YPos > passenger.DestinationYPos)
                {
                    if (car.YPos - passenger.StartingYPos < car.MoveBehavior.MoveAmount)
                    {
                        SlowDown(car, MoveDirection.South);
                    }
                    else
                    {
                        car.MoveDown();
                    }
                }
                //I personally think the passenger should have a currentX and currentY
                //that is incremented when the car moves so that when the user is dropped off
                //the passenger has current location x,y coordinates
                //else
                //{
                //    passenger.GetOutOfCar();
                //}
            }
        }

    }
}
