﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj
{
    public interface IMoveBehavior
    {
        int MoveUp(int yPos);

        int MoveDown(int yPos);

        int MoveRight(int xPos);

        int MoveLeft(int xPos);

        int MoveAmount { get; }
    }
}
